# С помощью include мы "встраиваем" код файла base.nim в код нашего модуля
# Это сделано для того, чтобы не нужно было писать множество импортов
include base

#[
  Модуль объявляется через module "Любое", "Количество", "Строк": код
  в данном случае первая строка - код смайлика, а вторая - само имя модуля 
  эти строки объединятся в одну при компиляции)
]#
module "&#8505;", "Пример модуля":
  #[ 
    command - объявление команд; при получении этих команд выполнится этот код 
    Внутри command доступны объекты: 
      msg: Message (объект сообщения),
      api: VkApi (объект для работы с VK API),
      text: string (все аргументы в одной строке)
      args: seq[string] (последовательность аргументов в виде строк)
  ]#
  command "тест", "test":
    #[
      Переменная usage - использование команды, выводится в команде "помощь"
      usage обязан быть константой (значение известно во время компиляции)
      usage также можно использовать в самом коде команды
    ]#
    usage = "тест <аргументы> - вывести полученные аргументы"
    const FormatString = "Это тестовая команда. Аргументы - $1\n Вложения - $2"
    # Получаем список приложений к сообщению через API
    let attaches = await msg.attaches(api)
    # Отвечаем пользователю. Процедура `%` - форматирование строки (из модуля strutils)
    answer(FormatString % [$args, $attaches])

  #[ 
    Секций "command" может быть сколько угодно.
    Все секции имеют отдельную область видимости переменных, так что
    нельзя, например, получить FormatString из команды выше
  ]#
  command "пример":
    usage = "пример - вывести `пример модуля`"
    # Благодаря синтаксису Nim при вызове процедуры можно убрать скобки
    answer "Это пример модуля!"